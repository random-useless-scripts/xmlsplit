#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef DEBUG
#define _(msg) printf("%d: %s\n", __LINE__, (msg))
#else
#define _(msg)
#endif

/* safe functions */

FILE *f_in;
FILE *f_out;

typedef struct {
	char *buf;
	size_t len;	/* how many characters, without \0 */
	size_t alloc;	/* size of *alloc'd memory */
} String;

void str_init(String *s)
{
	s->alloc = sizeof(char) * 32;
	s->buf = (char *) malloc(s->alloc);
	if (!s->buf) exit(EXIT_FAILURE);

	s->buf[0] = '\0';
	s->len = 0;
}

void str_appendc(String *s, char c)
{
	if (s->len + 1 >= s->alloc) {
		s->alloc += sizeof(char) * 32;
		s->buf = (char *) realloc(s->buf, s->alloc);
		if (!s->buf) exit(EXIT_FAILURE);
	}

	int len = strlen(s->buf);
	s->buf[len] = c;
	s->buf[len+1] = '\0';
	s->len = len+1;
}

String *getxmltag()
{
	char c = fgetc(f_in);

	while ( c != '<') {
		if (c == EOF) exit (EXIT_SUCCESS);
		c = fgetc(f_in);
	}
//	ungetc(c, f_in);


	String *s = (String *) malloc (sizeof (String));
	if (!s)
		exit (EXIT_FAILURE);

	str_init (s);

	while ( (c=fgetc(f_in)) != '>' ) {
		str_appendc(s,c);
	}

	return s;
}

void doit(void);

int options_count;
int invite_count;
int info_count;
int ok_count;
int trying_count;
int ack_count;


int main (int ac, char **av)
{
	/* be safe global pointers to 0 */
	f_in = f_out = 0;
	options_count = invite_count = info_count = ok_count = trying_count = ack_count = 0;

	if (ac < 2) {
		printf("give a filename\n");
		exit (EXIT_FAILURE);
	}
	f_in = fopen(av[1], "r");
	if (!f_in) {
		_("problem with fopen");
		exit(EXIT_FAILURE);
	}

	doit();

_("fclose");
	fclose (f_in);
	f_in = 0;
	return 0;
}

void str_clear(String *s)
{
	if (s && s->buf) {
		free(s->buf);
		s->buf = 0;
		s->alloc = 0;
		s->len = 0;
	}

}

#define XFOPEN(fn) while(0){f_out=fopen((fn),"w");if(!f_out)exit(EXIT_SUCCESS);}

void doit(void)
{
	String *s;
	s = 0;
	while (1) {
		str_clear(s);
		s = getxmltag();
_(s->buf);
		/* now apply filters */

		if (strstr(s->buf, "Line")) {
//_("line");
			fprintf(f_out, "%s\n", s->buf);
			continue;
		}

		if (strstr(s->buf, "OPTIONS")) {
//_("options");
			if (s->buf[0] == '/') {
				if (!f_out) {
					fclose (f_out);
					f_out = 0;
				}
			} else {
				char fn[100];
				sprintf(fn, "%s.%d.txt", "options", options_count++);
				f_out = fopen(fn, "w");
				if (!f_out) exit(EXIT_SUCCESS);
			}
			continue;
		}

		if (strstr(s->buf, "INVITE")) {
//_("invie");
			if (s->buf[0] == '/') {
				if (!f_out) {
					fclose (f_out);
					f_out = 0;
				}
			} else {
				char fn[100];
				sprintf(fn, "%s.%d.txt", "invite", invite_count++);
				f_out = fopen(fn, "w");
				if (!f_out) exit(EXIT_SUCCESS);			}
			continue;
		}

		if (strstr(s->buf, "INFO")) {
//_("info");
			if (s->buf[0] == '/') {
				if (!f_out) {
					fclose (f_out);
					f_out = 0;
				}
			} else {
				char fn[100];
				sprintf(fn, "%s.%d.txt", "info", info_count++);
				f_out = fopen(fn, "w");
				if (!f_out) exit(EXIT_SUCCESS);			}
			continue;
		}

		if (strstr(s->buf, "100_Trying")) {
//_("100");
			if (s->buf[0] == '/') {
				if (!f_out) {
					fclose (f_out);
					f_out = 0;
				}
			} else {
				char fn[100];
				sprintf(fn, "%s.%d.txt", "trying", trying_count++);
				f_out = fopen(fn, "w");
				if (!f_out) exit(EXIT_SUCCESS);			}
			continue;
		}

		if (strstr(s->buf, "200_OK")) {
//_("200");
			if (s->buf[0] == '/') {
				if (!f_out) {
					fclose (f_out);
					f_out = 0;
				}
			} else {
				char fn[100];
				sprintf(fn, "%s.%d.txt", "options", options_count++);
				f_out = fopen(fn, "w");
				if (!f_out) exit(EXIT_SUCCESS);			}
			continue;
		}

		if (strstr(s->buf, "_protocol_")) {
//_("protocol")
			if(options_count) {
				break;
			} else {
				continue;
			}
		}

		if (strstr(s->buf, "SIP") || strstr(s->buf, "message")) continue;

	}
}
